#include<iostream>
#include<fstream>

using namespace std;

int main()
{
    ofstream ofs("my.txt"); // to replace data from file (default arg: ios::trunc)
    // ofstream ofs("my.txt",ios::app); // to append data 
    ofs<<"Lalit"<<endl;
    ofs<<"Computer"<<endl;
    ofs<<"Sanghavi"<<endl;
    ofs<<565<<endl;
    ofs<<8469.11f;
    ofs.close();
}
