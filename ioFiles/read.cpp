#include<iostream>
#include<fstream>

using namespace std;

int main()
{
    ifstream ifs("my.txt");
    string name;
    string branch;
    string college;
    int roll;
    float prt;

    ifs>>name>>branch>>college>>roll>>prt;
    cout<<"name: "<<name<<endl;
    cout<<"branch: "<<branch<<endl;
    cout<<"college: "<<college<<endl;
    cout<<"exam no: "<<roll<<endl;
    cout<<"pointer: "<<prt<<endl;

    if(ifs.eof())
        ifs.close();
}
