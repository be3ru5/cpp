#include<iostream>
using namespace std;

// area of circle
// formula: pi(r*r)

int main()
{
    int r, area;
    cout<<"r: ";
    cin>>r;
    area = 3.14f*r*r;
    cout<<"area: "<<area;
    return 0;
}