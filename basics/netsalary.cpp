#include<iostream>
using namespace std;

// write a program to calculate Net Salary
// program should take following as input
// 1. basic salary
// 2. persentage of allowances
// 3. percentage of deductions

int main()
{
    int basic,allow,dedu,fsalary;

    cout<<"enter basic salary: ";
    cin>>basic;
    cout<<"enter allowances(%): ";
    cin>>allow;
    cout<<"enter deductions(%): ";
    cin>>dedu;

    fsalary= basic+(allow*basic/100)-(dedu*basic/100);
    cout<<"final salary: "<<fsalary;

    return 0;
}