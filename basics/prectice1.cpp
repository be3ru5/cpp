#include<iostream>
#include<cmath>
using namespace std;

int main()
{
    int u,v,a,x1,x2,y1,y2,r,h,amount,years;
    float speed,interest,rate,V,distance;

    // calculate distance
    // formula: vv-uu/2a (u-initial velocity,v-final velocity,a-aceleration)

    cout<<"enter u,v,a: ";
    cin>>u>>v>>a;
    speed = (v*v-u*u)/2*a;
    cout<<"speed: "<<speed;

    // calculate simple interest
    // formula: interest= (amount*rate*years)/100

    cout<<"enter amount,rate,years: ";
    cin>>amount>>rate>>years;
    interest = (amount*rate*years)/100;
    cout<<"interest: "<<interest;

    // calculate volume of a cylinder
    // formula: V=pi*r*r*h

    cout<<"enter r,h: ";
    cin>>r>>h;
    V=3.14f*r*r*h;
    cout<<V;

    // calculate distance between 2 points
    // formula: sqrt(sq(x2-x1)+sq(y2-y1))

    cout<<"enter x1,x2,y1,y2: ";
    cin>>x1>>x2>>y1>>y2;
    distance = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    cout<<"distance: "<<distance;

    return 0;
}
