#include<iostream>
using namespace std;

class MyException1 : exception
{
};

class MyException2 : public MyException1
{
};

int main()
{
    try
    {
        // throw 1;
        // throw MyException1();
        throw MyException2();
    }
    catch(MyException2 e)
    {
        cout<<"My Exception 2 "<<endl;
    }
    catch (MyException1 e)
    {
        cout<<"My Exception 1 "<<endl;
    }
    catch (...)
    {
        cout<<"remaining catch"<<endl;
    }
}
