#include<iostream>
using namespace std;

int main()
{
    // int a=10;
    // float a=3.5;
    // double a=3.5;
    // char a=3.5;    
    // string a="10.3";
 
    try
    {
        // throw a;
        throw 1.5f;
    }
    catch(int e)
    {
        cout<<"Int catch "<<e<<endl;
    }
    catch (double e)
    {
        cout<<"Double catch "<<e<<endl;
    }
    catch (char e)
    {
        cout<<"Char catch "<<endl;
    }
    catch (string e)
    {
        cout<<"String catch "<<endl;
    }
    catch (float e)
    {
        cout<<"Float catch "<<endl;
    }
    // catch (...)
    // {
    //     cout<<"remaining catch"<<endl;
    // }
}
