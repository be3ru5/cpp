#include<iostream>
using namespace std;

class Reactangle
{
public:
    float length;
    float breadth;

    float area ()
    {
        return length*breadth;
    }

    float perimeter()
    {
        return 2*(length+breadth);
    }
};

//regular declaration
// int main()
// {
//     Reactangle r1,r2;
//     r1.length=10;
//     r1.breadth=5;
//     cout<<"Area: "<<r1.area()<<endl;
//     r2.length=20;
//     r2.breadth=25;
//     cout<<"Area: "<<r2.perimeter()<<endl;
// }

//pointer to an onject
int main()
{
    Reactangle r1,r2;
    Reactangle *p;
    p=&r1;
    p->length=10;
    p->breadth=4;
    r2.length=12;
    r2.breadth=3;
    cout<<p->area()<<endl;
    cout<<r2.area();
} 
