#include<iostream>
#include<cstring>
using namespace std;

// student class
// inputs: roll nums, name, 3 subs marks
// functions: total_marks, grade
// grade: >75 A,>40 pass,<40 fail

class Student
{
private:
    int roll;
    int phy;
    int maths;
    int chem; 
    string name;
public:
    Student(int r,int p,int c,int m,string n)
    {
        roll=r;
        phy=p;
        chem=c;
        maths=m;
        name=n;
    }
    int total()
    {
        return phy+chem+maths;
    }
    char grade()
    {
        float avg=total()/3;
        if (avg>60)
            return 'A';
        else if (avg<=60 && avg>=40)
            return 'B';
        else
            return 'C';
    }
    void display()
    {
        cout<<name<<" ("<<roll<<") got "<<total()<<" marks and Grade: "<<grade()<<endl;
        // cout<<"grade: "<<grade()<<endl;
    }
};

int main()
{
    int r;
    int p,c,m;
    string name;
    cout<<"roll no.: ";
    cin>>r;
    cout<<"name: ";
    cin>>name;
    cout<<"marks (p,c,m): ";
    cin>>p>>c>>m;

    Student s(r,p,c,m,name);
    s.display();
}
