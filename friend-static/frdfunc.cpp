#include<iostream>
using namespace std;

class Test
{
    private: int a;
    protected: int b;
    public: int c;
    friend int main();
};

int main()
{
    Test t;
    t.a=15;
    t.b=10;
    t.c=5;
}
