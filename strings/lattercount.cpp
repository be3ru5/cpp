#include<iostream>
#include<string>
using namespace std;

int main()
{
    string str="how Many wOrds";
    // 3,9,3,2
    string::iterator it;
    int vowels=0,conso=0,space=0;

    for (it=str.begin();it!=str.end();it++)
    {
    // cout<<*it<<endl;
    // a=65,97 e=69,101 i=73,105 o=79,111 u=85,117
    // space=32
        if (*it==65||*it==97||*it==69||*it==101||*it==73||*it==105||*it==79||*it==111||*it==85||*it==117)
        {
            vowels++;
        }    
        else if (*it==32)
        {
            space++;
        }
        else
        {
            conso++;
        }
    }

    cout<<"vowels \t\t: "<<vowels<<endl;
    cout<<"consonants \t: "<<conso<<endl;
    cout<<"words \t\t: "<<space+1<<endl;
    cout<<"spaces \t\t: "<<space<<endl;

    return 0;
}