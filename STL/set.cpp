#include<iostream>
#include<set>
using namespace std;

int main()
{
    set<int> v={2,4,6,8,10};
    v.insert(20);
    v.insert(30);
    v.insert(6);    // dublicate values are not allowed
    v.insert(2);    // dublicate values are not allowed

    cout<<"using iterator"<<endl;
    set<int>::iterator itr;
    for (itr=v.begin();itr!=v.end();itr++)
        // cout<<++*itr<<" ";
        cout<<*itr<<" ";    // cannot modify

    cout<<endl<<"using for each loop"<<endl;
    for (int x:v)
        cout<<x<<" ";
}
