#include<iostream>
#include<list>
using namespace std;

int main()
{
    list<int> v={2,4,6,8,10};
    v.push_back(20);
    v.push_back(30);
    // v.pop_back();

    cout<<"using iterator"<<endl;
    list<int>::iterator itr;
    for (itr=v.begin();itr!=v.end();itr++)
        cout<<++*itr<<" ";

    cout<<endl<<"using for each loop"<<endl;
    for (int x:v)
        cout<<x<<" ";
}
