#include<iostream>
using namespace std;

class Base
{
public:
    Base()
    {
        cout<<"Base constructor"<<endl;
    }
    virtual ~Base()     // virtual destructor 
    {
        cout<<"Base destructor"<<endl;
    }
};

class Derived : public Base
{
public:
    Derived()
    {
        cout<<"Derived constructor"<<endl;
    }
    ~Derived()
    {
        cout<<"Derived destructor"<<endl;
    }
};

int main()
{
    Base *p=new Derived();
    delete p;
}
