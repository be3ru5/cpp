#include<iostream>
using namespace std;

class Demo
{
public:
    Demo()
    {
        cout<<"constructor"<<endl;
    }
    ~Demo()     // destructor
    {
        cout<<"destructor"<<endl;
    }
};

void func()
{
    Demo *p=new Demo;
    delete p;
}

int main()
{
    func();
}
