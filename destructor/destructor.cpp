#include<iostream>
using namespace std;

class Demo
{
public:
    Demo()
    {
        cout<<"constructor"<<endl;
    }
    ~Demo()     // destructor
    {
        cout<<"destructor"<<endl;
    }
};

void func()
{
    Demo d;
}

int main()
{
    func();
}
