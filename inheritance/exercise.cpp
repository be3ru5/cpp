// write a class for employee
// derived classed:
//     1. fulltime employee with salary
//     2. part time employee with daily wages
// write required methods
// emp_id, emp_name, emp_salary

#include<iostream>
using namespace std;

class Employee
{
private:
    int id;
    string name;
public:
    Employee(int no, string na)
    {
        id=no;
        name=na;
    }
    int getID(){return id;}
    string getName(){return name;}
};

class FullTime:public Employee
{
private: int salary;
public:
    FullTime(int no,string na,int sal):Employee(no,na)
    {
        salary=sal;
    }
    int getSalary(){return salary;}
};

class PartTime:public Employee
{
private: int wage;
public:
    PartTime(int no,string na,int wg):Employee(no,na)
    {
        wage=wg;
    }
    int getWage(){return wage;}
};

int main()
{
    PartTime p1(1,"Lalit",1000);
    FullTime p2(2,"Khairnar",50000);

    cout<<"salary of "<<p2.getName()<<" ("<<p2.getID()<<") is "<<p2.getSalary()<<endl;
    cout<<"Daily wage of "<<p1.getName()<<" ("<<p1.getID()<<") is "<<p1.getWage()<<endl;
}
