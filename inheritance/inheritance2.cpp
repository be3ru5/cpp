#include<iostream>
using namespace std;

class Rectangle
{
private:
    int lenght;
    int breadth;
public:
    Rectangle();                // non-parameterized construction
    Rectangle(int l,int b);     // parameterized construction
    Rectangle(Rectangle &r);    // copy construction
    int getLenght();            // mutator
    int getBreadth();           // mutator
    void setLenght(int l);      // accessor
    void setBreadth(int b);     // accessor
    int area();                 // facilitator
    int perimeter();            // facilitator
    bool isSquare();            // enquiry
    ~Rectangle();               // destructor
};

class Cuboid:public Rectangle
{
private:
    int height;
public:
    Cuboid (int h)
    {
        height=h;
    }
    int getHeight()
    {
        return height;
    }
    void setHeight(int h)
    {
        height=h;
    }
    int volume()
    {
        return getLenght()*getBreadth()*height;
    }
};

int main()
{
    Cuboid c(5);
    c.setLenght(12);
    c.setBreadth(3);
    cout<<c.volume()<<endl;
}

Rectangle::Rectangle()
{
    lenght=1;
    breadth=1;    
}

Rectangle::Rectangle(int l, int b)
{
    lenght=l;
    breadth=b;    
}

Rectangle::Rectangle(Rectangle &r)
{
    lenght=r.lenght;
    breadth=r.breadth;
}

int Rectangle::getLenght()
{
    return lenght;
}

int Rectangle::getBreadth()
{
    return breadth;
}

void Rectangle::setLenght(int l)
{
    lenght=l;
}

void Rectangle::setBreadth(int b)
{
    breadth=b;
}

int Rectangle::area()
{
    return lenght*breadth;
}

int Rectangle::perimeter()
{
    return 2*(lenght+breadth);
}

bool Rectangle::isSquare()
{
    return lenght=breadth;
}

Rectangle::~Rectangle()
{
    cout<<"Rectangle Destroyed";
}
