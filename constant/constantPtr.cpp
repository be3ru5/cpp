#include<iostream>
using namespace std;

int main()
{
    int x=10;
    const int *ptr=&x;
    cout<<ptr<<endl;
    // ++(*ptr); // constant pointer cannot modify value of a variable

    int y=12;
    ptr=&y;        // can modify the value of the pointer
    cout<<ptr;
}
