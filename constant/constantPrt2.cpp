#include<iostream>
using namespace std;

int main()
{
    int x=10;
    int *const ptr=&x;
    cout<<ptr<<endl;

    int y=12;
    // ptr=&y;      // cannot modify the value of the constant pointer
    ++(*ptr);       // can modify value of a pointing variable
    cout<<ptr;
}
